<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\BlogPost;
use DB;

class BlogPostsController extends Controller
{
  protected $model;
  protected $request;
  protected $fields;
  protected $datarequest;

  public function __construct(BlogPost $blogpost, Request $request)
  {
    $this->model                =  $blogpost;
    $this->request              =  $request;

    $this->datarequest = [
      'titulo'               =>  'Blog - Posts',
      'diretorio'            =>  'backend.blog.posts',
      'url_action'               =>  'admin/blog/posts'
    ];
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    try {
      $order = $this->request->input('order') == 'asc' ? 'desc' : 'asc';
      $column_name = null;

      if ($this->request->input('column')) {
        $column = $this->request->input('column');
        $column_name = "$column $order";
      } else {
        $column_name = "a.id desc";
      }

      $field = $this->request->input('field') ? $this->request->input('field') : 'a.title';
      $operador = $this->request->input('operador') ? $this->request->input('operador') : 'like';
      $value = $this->request->input('value') ? $this->request->input('value') : '';

      if ($field == 'data' || $field == 'dataini' || $field == 'datafim') {
        $value = Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d');
      }

      if ($operador == 'like') {
        $newValue = "'%$value%'";
      } else {
        $newValue = "'$value'";
      }

      $results = DB::table('blog_posts as a')
        ->select('a.id as id_post', 'a.title', 'a.category_id', 'a.views', 'a.description', 'a.slug as slugPost', 'a.status', 'a.created_at', 'b.id', 'b.name as category_name', 'b.slug as slugCategory')
        ->orderByRaw("$column_name")
        ->whereraw("$field $operador $newValue")
        ->join('blog_categories as b', 'b.id', '=', 'a.category_id')
        ->paginate(10);

        $getCategories = DB::table('blog_categories')
        ->select('id', 'name', 'status')
        ->orderBy('id', 'desc')
        ->get();


    } catch (\Exception $err) {
      return response()->json($err->getMessage(), 500);
    }

    return view($this->datarequest['diretorio'] . '.index', compact('results', 'order', 'getCategories'))->with($this->datarequest);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $getCategories = DB::table('blog_categories')
    ->select('id', 'name', 'status')
    ->orderBy('id', 'desc')
    ->get();

    return view($this->datarequest['diretorio'] . '.form', compact('getCategories'))->with($this->datarequest);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store()
  {
    $model = new $this->model;
    $result = $this->request->all();

    $rules = [
      'title'     => "required",
      'category_id'     => "required",
      'slug' => 'required'
    ];

    $messages = [
      'title.required' => 'título é obrigatório',
      'category_id.required' => 'categoria é obrigatório',
      'slug.required' => 'url amigável é obrigatório'
    ];

    $validator = Validator::make($result, $rules, $messages);

    if ($validator->fails()) {
      return response()->json($validator->errors()->first(), 422);
    }

    $model->title = $result['title'];
    $model->category_id = $result['category_id'];
    $model->description = $result['description'];
    $model->slug = $result['slug'];
    $model->status = $result['status'];
    $model->views = $result['views'];
    $model->created_at = Carbon::now();
    $model->updated_at = Carbon::now();

    try {
      $model->save();
    } catch (\Exception $e) {
      \Log::error($e->getMessage());
      return response()->json($e->getMessage(), 500);
    }

    return response()->json('Postagem cadastrada com sucesso', 200);
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\Service  $service
   * @return \Illuminate\Http\Response
   */
  public function show($slugCategory, $slugPost)
  {
    $result = DB::table('blog_posts as a')
    ->select('a.title', 'a.description', 'a.slug as slugPost', 'a.category_id', 'b.name as nameCategory', 'b.slug as slugCategoria')
    ->join('blog_categories as b', 'b.id', '=', 'a.category_id')
    ->where('a.slug', $slugPost)
    ->first();
   
    return view($this->datarequest['diretorio'] . '.details', compact('result'))->with($this->datarequest);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  \App\Service  $service
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $getCategories = DB::table('blog_categories')
    ->select('id', 'name', 'status')
    ->orderBy('id', 'desc')
    ->get();

    $result = $this->model::where('id', $id)->first();
    return view($this->datarequest['diretorio'] . '.form', compact('result', 'getCategories'))->with($this->datarequest);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \App\Service  $service
   * @return \Illuminate\Http\Response
   */
  public function update($id)
  {
    $model = $this->model::find($id);
    $result = $this->request->all();

    $rules = [
      'title'     => "required",
      'category_id'     => "required",
      'slug' => 'required'
    ];

    $messages = [
      'title.required' => 'título é obrigatório',
      'category_id.required' => 'categoria é obrigatório',
      'slug.required' => 'url amigável é obrigatório'
    ];

    $validator = Validator::make($result, $rules, $messages);

    if ($validator->fails()) {
      return response()->json($validator->errors()->first(), 422);
    }

    $model->title = $result['title'];
    $model->category_id = $result['category_id'];
    $model->description = $result['description'];
    $model->slug = $result['slug'];
    $model->status = $result['status'];
    $model->views = $result['views'];
    $model->updated_at = Carbon::now();

    try {
      $model->save();
    } catch (\Exception $e) {
      \Log::error($e->getMessage());
      return response()->json($e->getMessage(), 500);
    }

    return response()->json('Postagem alterada com sucesso', 200);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\Service  $service
   * @return \Illuminate\Http\Response
   */
  public function destroy()
  {
    $model = new $this->model;
    $data = $this->request->all();

    if (!isset($data['selected'])) {
      return response()->json('Selecione ao menos um registro', 422);
    }

    try {
      foreach ($data['selected'] as $result) {
        $find = $model->find($result);
        $find->delete();
      }
    } catch (\Exception $e) {
      \Log::error($e->getMessage());
      return response()->json($e->getMessage(), 500);
    }

    return response()->json(true, 200);
  }
}

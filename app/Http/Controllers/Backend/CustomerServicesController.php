<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\CustomerServices;
use App\Models\Invoice;
use DB;
use Dotenv\Result\Result;

class CustomerServicesController extends Controller
{
  protected $model;
  protected $request;
  protected $fields;
  protected $datarequest;

  public function __construct(CustomerServices $customerservice, Request $request)
  {
    $this->model                =  $customerservice;
    $this->request              =  $request;

    $this->datarequest = [
      'titulo'               =>  'Serviços de Clientes',
      'diretorio'            =>  'backend.customerservices',
      'url_action'               =>  'admin/customerservices'
    ];
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    try {
      $order = $this->request->input('order') == 'asc' ? 'desc' : 'asc';
      $column_name = null;

      if ($this->request->input('column')) {
        $column = $this->request->input('column');
        $column_name = "$column $order";
      } else {
        $column_name = "id desc";
      }

      $field = $this->request->input('field') ? $this->request->input('field') : 'name';
      $operador = $this->request->input('operador') ? $this->request->input('operador') : 'like';
      $value = $this->request->input('value') ? $this->request->input('value') : '';

      if ($field == 'data' || $field == 'dataini' || $field == 'datafim') {
        $value = Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d');
      }

      if ($operador == 'like') {
        $newValue = "'%$value%'";
      } else {
        $newValue = "'$value'";
      }

      $results = DB::table('customerservices')
        ->select('id', 'name', 'company', 'email', 'status', 'phone', 'created_at')
        ->orderByRaw("$column_name")
        ->whereraw("$field $operador $newValue")
        ->paginate(10);
    } catch (\Exception $err) {
      return response()->json($err->getMessage(), 500);
    }

    return view($this->datarequest['diretorio'] . '.index', compact('results', 'order'))->with($this->datarequest);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create($customer_id)
  {
    $getServices = DB::table('services as a')
      ->where('a.status', 'Ativo')
      ->get();
    return view('backend.customers.servicesForm', compact('getServices', 'customer_id'))->with($this->datarequest);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store()
  {
    $model = new $this->model;
    $result = $this->request->all();

    $rules = [
      'service_id' => 'required',
      'date_start' => 'required',
      'date_end' => 'required',
      'price' => 'required'
    ];

    $messages = [
      'service_id.required' => 'selecione um serviço',
      'date_start.required' => 'data de início é obrigatório',
      'date_end.required' => 'data de vencimento é obrigatório',
      'price.min' => 'preço é obrigatório'
    ];

    $validator = Validator::make($result, $rules, $messages);

    if ($validator->fails()) {
      return response()->json($validator->errors()->first(), 422);
    }

    $model->customer_id = $result['customer_id'];
    $model->service_id = $result['service_id'];
    $model->dominio = $result['dominio'];
    $model->status = $result['status'];
    $model->date_start = $result['date_start'];
    $model->date_end = $result['date_end'];
    $model->price = $result['price'];
    $model->period = $result['period'];
    $model->ftp = $result['ftp'];
    $model->ftp_login = $result['ftp_login'];
    $model->ftp_password = $result['ftp_password'];
    $model->created_at = Carbon::now();
    $model->updated_at = Carbon::now();

    $msg_success = '';
    try {
      $model->save();
      $getService = DB::table('services')->where('id', $result['service_id'])->first();

      DB::table('customer_activities')->insert([
        'customer_id' => $result['customer_id'],
        'description' => 'Novo serviço: ' . $getService->name . '',
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now(),
      ]);

      // Nesse momento preciso disparar o Evento de Gerar Fatura
      if (isset($result['generate_invoice'])) {
        $msg_success = ' e fatura gerada.';
        $customer_service_id = $model->id;
        $getCustomer = DB::table('customers')->where('id', $result['customer_id'])->first();
        $period = null;
        if ($result['period'] == 'unico') {
          $description = $getService->name . ' - ' . $result['dominio'] . ' - pagamento unico';
        } else {
          if ($result['period'] == 'mensal') {
            $period = 1;
          } else if ($result['period'] == 'trimestral') {
            $period = 3;
          } else if ($result['period'] == 'anual') {
            $period = 12;
          }
          $description = $getService->name . ' - ' . $result['dominio'] . ' (de: ' . Carbon::parse($result['date_end'])->format('d/m/Y') . ' até ' . Carbon::parse($result['date_end'])->addMonth($period)->format('d/m/Y') . ') - R$ ' . number_format($result['price'], 2);
        }

        $array_descriptions = array(
          $description
        );
        $array_services = array(
          $customer_service_id
        );

        $invoice = new Invoice;
        $invoice->customer_id = $result['customer_id'];
        $invoice->customer_service_id = json_encode($array_services);
        $invoice->description = $array_descriptions;
        $invoice->price = $result['price'];
        $invoice->status = 'nao_pago';
        $invoice->payment_method = $getCustomer->payment_method;
        $invoice->date_invoice = Carbon::now();
        $invoice->date_end = Carbon::now()->addDays(7);
        $invoice->date_payment = null;
        $invoice->created_at = Carbon::now();
        $invoice->updated_at = Carbon::now();
        $invoice->save();

        DB::table('customer_activities')->insert([
          'customer_id' => $result['customer_id'],
          'description' => 'Fatura #' . $invoice->id . ' gerada',
          'created_at' => Carbon::now()->addMinutes(1),
          'updated_at' => Carbon::now()->addMinutes(1),
        ]);

        $data_fatura = Carbon::now()->format('d/m/Y');
        if (isset($result['send_invoice'])) {
          $details = [
            'title' => 'Nova fatura gerada',
            'customer' => $getCustomer->name,
            'company' => $getCustomer->company,
            'data_fatura' => $data_fatura,
            'data_vencimento' => Carbon::now()->addDays(7)->format('d/m/Y'),
            'price' => number_format($result['price'], 2),
            'payment_method' => $getCustomer->payment_method,
            'description' => [
              $description
            ],
            'invoice_id' => $invoice->id,
            'url_base' => url('/')
          ];

          \Mail::to($getCustomer->email)->send(new \App\Mail\NewInvoice($details));
        }
      }
    } catch (\Exception $e) {
      \Log::error($e->getMessage());
      return response()->json($e->getMessage(), 500);
    }

    return response()->json('Serviço cadastrado com sucesso' . $msg_success, 200);
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\CustomerServices  $customerservice
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    // OFF SEM UTILIZAÇÃO
    // OFF SEM UTILIZAÇÃO
    // OFF SEM UTILIZAÇÃO
    // OFF SEM UTILIZAÇÃO
    // OFF SEM UTILIZAÇÃO
    $result = $this->model::find($id);
    $allCustomerServicess = $this->model::where('id', '!=', $id)->get();

    $myServices = DB::table('customerservice_services as a')
      ->select('a.id as id', 'a.customerservice_id', 'a.service_id', 'a.dominio', 'a.date_start', 'a.date_end', 'a.price', 'a.period', 'a.ftp', 'a.ftp_login', 'a.ftp_password', 'a.status', 'b.name as nameService')
      ->join('services as b', 'a.service_id', 'b.id')
      // ->join('categories as c', 'b.category_id', 'c.id')
      // ->leftjoin('plans as d', 'a.plan_id', 'd.id')
      ->where('a.customerservice_id', $id)
      // ->where('d.upgrade', 0)
      // ->where('d.statistics', 1)
      // ->orderBy('b.views', 'DESC')
      ->paginate(10);
    // dd($myServices);
    return view($this->datarequest['diretorio'] . '.details', compact('result', 'allCustomerServicess', 'myServices'))->with($this->datarequest);
    // OFF SEM UTILIZAÇÃO
    // OFF SEM UTILIZAÇÃO
    // OFF SEM UTILIZAÇÃO
    // OFF SEM UTILIZAÇÃO
    // OFF SEM UTILIZAÇÃO
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  \App\CustomerServices  $customerservice
   * @return \Illuminate\Http\Response
   */
  public function edit($customer_id, $id)
  {
    $result = $this->model::where('id', $id)->first();
    $getServices = DB::table('services as a')
      ->where('a.status', 'Ativo')
      ->get();

    return view('backend.customers.servicesForm', compact('result', 'getServices', 'customer_id'))->with($this->datarequest);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \App\CustomerServices  $customerservice
   * @return \Illuminate\Http\Response
   */
  public function update($id)
  {
    $model = $this->model::find($id);
    $result = $this->request->all();

    $validator = Validator::make($result, [
      'dominio'     => "required",
      'date_start' => 'required',
      'date_end' => 'required',
      'price' => 'required'
    ]);

    if ($validator->fails()) {
      return response()->json($validator->errors()->first(), 422);
    }

    $model->customer_id = $result['customer_id'];
    $model->service_id = $result['service_id'];
    $model->dominio = $result['dominio'];
    if (isset($result['status'])) {
      $model->status = $result['status'];
    } else {
      $model->status = 'Pendente';
    }
    $model->date_start = $result['date_start'];
    $model->date_end = $result['date_end'];
    $model->price = $result['price'];
    $model->period = $result['period'];
    $model->ftp = $result['ftp'];
    $model->ftp_login = $result['ftp_login'];
    $model->ftp_password = $result['ftp_password'];
    $model->updated_at = Carbon::now();

    try {
      $model->save();
    } catch (\Exception $e) {
      \Log::error($e->getMessage());
      return response()->json($e->getMessage(), 500);
    }

    return response()->json('Serviço salvo com sucesso', 200);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\CustomerServices  $customerservice
   * @return \Illuminate\Http\Response
   */
  public function destroy()
  {
    $model = new $this->model;
    $data = $this->request->all();

    if (!isset($data['selectedServices'])) {
      return response()->json('Selecione ao menos um serviço', 422);
    }

    try {
      foreach ($data['selectedServices'] as $result) {
        $find = $model->find($result);
        $find->delete();
      }
    } catch (\Exception $e) {
      \Log::error($e->getMessage());
      return response()->json($e->getMessage(), 500);
    }

    return response()->json(true, 200);
  }
}

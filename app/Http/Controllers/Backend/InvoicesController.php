<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Invoice;
use App\Models\CustomerServices;
use DB;

class InvoicesController extends Controller
{
  protected $model;
  protected $request;
  protected $fields;
  protected $datarequest;

  public function __construct(Invoice $invoice, Request $request)
  {
    $this->model                =  $invoice;
    $this->request              =  $request;

    $this->datarequest = [
      'titulo'               =>  'Todas as Faturas',
      'diretorio'            =>  'backend.invoices',
      'url_action'               =>  'admin/invoices'
    ];
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    try {
      $order = $this->request->input('order') == 'asc' ? 'desc' : 'asc';
      $column_name = null;

      if ($this->request->input('column')) {
        $column = $this->request->input('column');
        $column_name = "$column $order";
      } else {
        $column_name = "id desc";
      }

      $field = $this->request->input('field') ? $this->request->input('field') : 'date_invoice';
      $operador = $this->request->input('operador') ? $this->request->input('operador') : 'like';
      $value = $this->request->input('value') ? $this->request->input('value') : '';

      if ($field == 'data' || $field == 'dataini' || $field == 'datafim') {
        $value = Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d');
      }

      if ($operador == 'like') {
        $newValue = "'%$value%'";
      } else {
        $newValue = "'$value'";
      }

      $results = DB::table('invoices as a')
        ->select('a.id', 'a.customer_id', 'a.description', 'a.price', 'a.payment_method', 'a.date_invoice', 'a.date_end', 'a.date_payment', 'a.status', 'b.name as nameCustomer')
        ->join('customers as b', 'a.customer_id', 'b.id')
        // ->join('categories as c', 'b.category_id', 'c.id')
        // ->leftjoin('plans as d', 'a.plan_id', 'd.id')
        ->orderByRaw("$column_name")
        ->whereraw("$field $operador $newValue")
        ->paginate(10);
    } catch (\Exception $err) {
      return response()->json($err->getMessage(), 500);
    }

    return view($this->datarequest['diretorio'] . '.index', compact('results', 'order'))->with($this->datarequest);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create($customer_id)
  {
    return view('backend.customers.invoicesForm', compact('customer_id'))->with($this->datarequest);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store()
  {
    $model = new $this->model;
    $result = $this->request->all();
    $msg_success = '';

    $validator = Validator::make($result, [
      'description'     => "required",
      'price' => 'required',
      'date_invoice' => 'required',
      'date_end' => 'required'
    ]);

    if ($validator->fails()) {
      return response()->json($validator->errors()->first(), 422);
    }

    $array_descriptions = array(
      $result['description']
    );

    $model->customer_id = $result['customer_id'];
    $model->description = $array_descriptions;
    $model->price = $result['price'];
    $model->status = $result['status'];
    $model->payment_method = $result['payment_method'];
    $model->date_invoice = $result['date_invoice'];
    $model->date_end = $result['date_end'];
    $model->date_payment = $result['date_payment'];
    $model->created_at = Carbon::now();
    $model->updated_at = Carbon::now();

    try {
      $model->save();

      if (isset($result['send_invoice'])) {
        $getCustomer = DB::table('customers')->where('id', $model->customer_id)->first();

        $msg_success = ' e E-mail encaminhado para o Cliente';

        $data_fatura = Carbon::now()->format('d/m/Y');

        $details = [
          'title' => 'Nova fatura gerada',
          'customer' => $getCustomer->name,
          'company' => $getCustomer->company,
          'data_fatura' => $data_fatura,
          'data_vencimento' => Carbon::parse($model->date_end)->addDays(7)->format('d/m/Y'),
          'data_pagamento' => Carbon::now()->format('d/m/Y H:i'),
          'price' => $model->price,
          'payment_method' => $getCustomer->payment_method,
          'description' => $model->description,
          'invoice_id' => $model->id,
          'url_base' => url('/')
        ];

        \Mail::to($getCustomer->email)->send(new \App\Mail\NewInvoice($details));
      }
    } catch (\Exception $e) {
      \Log::error($e->getMessage());
      return response()->json($e->getMessage(), 500);
    }
    return response()->json('Fatura salva com sucesso' . $msg_success, 200);
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\Invoice  $invoice
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $result = $this->model::find($id);
    $getCustomer = DB::table('customers')->where('id', $result->customer_id)->first();

    return view($this->datarequest['diretorio'] . '.details', compact('result', 'getCustomer'))->with($this->datarequest);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  \App\Invoice  $invoice
   * @return \Illuminate\Http\Response
   */
  public function edit($customer_id, $id)
  {
    $result = $this->model::where('id', $id)->first();
    return view('backend.customers.invoicesForm', compact('result', 'customer_id'))->with($this->datarequest);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \App\Invoice  $invoice
   * @return \Illuminate\Http\Response
   */
  public function update($id)
  {
    $model = $this->model::find($id);
    $result = $this->request->all();

    $validator = Validator::make($result, [
      // 'description'     => "required",
      'price' => 'required',
      'date_invoice' => 'required',
      'date_end' => 'required'
    ]);

    if ($validator->fails()) {
      return response()->json($validator->errors()->first(), 422);
    }

    $model->customer_id = $result['customer_id'];
    // $model->description = $result['description'];
    $model->price = $result['price'];
    $model->status = $result['status'];
    $model->payment_method = $result['payment_method'];
    $model->date_invoice = $result['date_invoice'];
    $model->date_end = $result['date_end'];
    $model->date_payment = $result['date_payment'];
    $model->updated_at = Carbon::now();

    try {
      $model->save();
    } catch (\Exception $e) {
      \Log::error($e->getMessage());
      return response()->json($e->getMessage(), 500);
    }

    return response()->json('Fatura salva com sucesso', 200);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\Invoice  $invoice
   * @return \Illuminate\Http\Response
   */
  public function destroy()
  {
    $model = new $this->model;
    $data = $this->request->all();

    if (!isset($data['selectedInvoices'])) {
      return response()->json('Selecione ao menos uma fatura', 422);
    }

    try {
      foreach ($data['selectedInvoices'] as $result) {
        $find = $model->find($result);
        $find->delete();
      }
    } catch (\Exception $e) {
      \Log::error($e->getMessage());
      return response()->json($e->getMessage(), 500);
    }

    return response()->json(true, 200);
  }

  public function confirm(Request $request, $id)
  {
    $model = $this->model::find($id);
    $result = $this->request->all();
    $getCustomer = DB::table('customers')->where('id', $model->customer_id)->first();
    $msg_success = '';
    
    $model->status = 'pago';
    $model->date_payment = Carbon::now();
    $model->updated_at = Carbon::now();
    
    
    try {
      $model->save();

      if (isset($model->customer_service_id)) {
        $services_id = json_decode($model->customer_service_id);
        foreach ($services_id as $service_id) {
          $customer_service = new CustomerServices;
          $up_customer_service = $customer_service->find($service_id);

          if ($up_customer_service->period != 'unico') {
            if ($up_customer_service->period == 'mensal') {
              $period = 1;
            } else if ($up_customer_service->period == 'trimestral') {
              $period = 3;
            } else if ($up_customer_service->period == 'anual') {
              $period = 12;
            }
            $new_date_end = Carbon::parse($up_customer_service->date_end)->addMonth($period);

            $up_customer_service->date_end = $new_date_end;
            $up_customer_service->status = 'ativo';
            $up_customer_service->save();
          }
        }
      }


        DB::table('customer_activities')->insert([
          'customer_id' => $model->customer_id,
          'description' => 'Pagamento da Fatura #' . $id,
          'created_at' => Carbon::now(),
          'updated_at' => Carbon::now(),
        ]);

        if (isset($result['sendmailinvoice'])) {
          $msg_success = ' e E-mail encaminhado para o Cliente';

          $data_fatura = Carbon::parse($model->date_end)->format('d/m/Y');

          $details = [
            'title' => 'Confirmação de Pagamento',
            'customer' => $getCustomer->name,
            'company' => $getCustomer->company,
            'data_fatura' => $data_fatura,
            'data_vencimento' => Carbon::parse($model->date_end)->format('d/m/Y'),
            'data_pagamento' => Carbon::now()->format('d/m/Y H:i'),
            'price' => number_format($model->price, 2),
            'payment_method' => $getCustomer->payment_method,
            'description' => $model->description,
            'invoice_id' => $model->id,
            'status_payment' => 'Pago',
            'url_base' => url('/')
          ];

          \Mail::to($getCustomer->email)->send(new \App\Mail\InvoicePay($details));
        }
      
    } catch (\Exception $e) {
      \Log::error($e->getMessage());
      return response()->json($e->getMessage(), 500);
    }

    return response()->json('Fatura Confirmada com sucesso' . $msg_success, 200);
  }
}

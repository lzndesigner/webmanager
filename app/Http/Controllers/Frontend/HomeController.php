<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Invoice;
use DB;
use Carbon\Carbon;

class HomeController extends Controller
{
  // Dashboard
  public function index()
  {
    // return view('frontend.home');

    $date_now = Carbon::now();
    $date_invoice_generate = Carbon::now()->addDays(7);

    // busca quais são os serviços a vencer e pega o ID do cliente
    $getServiceDues = DB::table('customer_services')
      ->select('customer_id')
      ->where('period', '!=', 'unico')
      ->where('status', 'ativo')
      ->where('date_end', '<=', $date_invoice_generate)
      ->groupBy('customer_id')
      ->get();


    // Verifica se há serviços a vencer
    if ($getServiceDues) {
      foreach ($getServiceDues as $getServiceDue) {

        $ar_invoice = [];
        $invoice_price = 0;

        $ar_invoice['invoice_info'] = [
          'date_invoice' => $date_now,
          'date_end' => Carbon::parse($date_now)->addDays(7),
        ];

        $getCustomer = DB::table('customers')->where('id', $getServiceDue->customer_id)->first();

        $ar_invoice['customer_info'] = [
          'customer_id' => $getServiceDue->customer_id,
          'customer_name' => $getCustomer->name,
          'customer_company' => $getCustomer->company,
          'customer_email' => $getCustomer->email,
          'customer_payment' => $getCustomer->payment_method
        ];

        $getServicesDues = DB::table('customer_services')
          ->where('customer_id', $getServiceDue->customer_id)
          ->where('period', '!=', 'unico')
          ->where('status', 'ativo')
          ->where('date_end', '<=', $date_invoice_generate)
          ->get();

        foreach ($getServicesDues as $getServicesDue) {
          $verifyInvoices = DB::table('invoices')
            ->where('customer_id', $getCustomer->id)
            ->where('date_payment', null)
            ->where('status', 'nao_pago')
            ->first();

          if ($verifyInvoices == null) {

            switch ($getServicesDue->period) {
              case 'mensal':
                $period = 1;
                break;
              case 'trimestral':
                $period = 3;
                break;
              case 'anual':
                $period = 12;
                break;
              default:
                $period = 0;
            }

            $data_fatura = Carbon::parse($getServicesDue->date_end)->format('d/m/Y');
            $proxima_data_vencimento = Carbon::parse($getServicesDue->date_end)->addMonth($period)->format('d/m/Y');

            $getService = DB::table('services')->where('id', $getServicesDue->service_id)->first();

            $ar_invoice['customer_service'][] = [
              'customer_service_id' => $getServicesDue->id,
              'customer_service_description' => $getService->name . ' - ' . $getServicesDue->dominio . ' (de: ' . $data_fatura . ' até ' . $proxima_data_vencimento . ') - R$ '. $getServicesDue->price,
              'customer_service_price' => $getServicesDue->price
            ];
            $invoice_price += $getServicesDue->price;
          } // if verifique invoice == null
        } // foreach em cada service

        // dd($ar_invoice);

        if (isset($ar_invoice['customer_service'])) {

          $array_descriptions = array();
          $array_services = array();

          foreach ($ar_invoice['customer_service'] as $invoice_services) {
            array_push($array_descriptions, $invoice_services['customer_service_description']);
            array_push($array_services, $invoice_services['customer_service_id']);
            // dd($array_descriptions);
          }
          // dd(json_encode($array_services));
          
          $newInvoice = DB::table('invoices')->insertGetId([
            'customer_id' => $ar_invoice['customer_info']['customer_id'],
            'customer_service_id' => json_encode($array_services),
            'description' => json_encode($array_descriptions),
            'price' => $invoice_price,
            'payment_method' => $ar_invoice['customer_info']['customer_payment'],
            'date_invoice' => $ar_invoice['invoice_info']['date_invoice'],
            'date_end' => Carbon::parse($ar_invoice['invoice_info']['date_invoice'])->addDays(7),
            'date_payment' => null,
            'status' => 'nao_pago',
            'created_at' => $ar_invoice['invoice_info']['date_invoice'],
            'updated_at' => $ar_invoice['invoice_info']['date_invoice']
          ]);

          DB::table('customer_activities')->insert([
            'customer_id' => $ar_invoice['customer_info']['customer_id'],
            'description' => 'Fatura #' . $newInvoice . ' gerada',
            'created_at' => $ar_invoice['invoice_info']['date_invoice'],
            'updated_at' => $ar_invoice['invoice_info']['date_invoice'],
          ]);

          $details = [
            'title' => 'Nova fatura gerada',
            'customer' => $ar_invoice['customer_info']['customer_name'],
            'company' => $ar_invoice['customer_info']['customer_company'],
            'data_fatura' => Carbon::parse($ar_invoice['invoice_info']['date_invoice'])->format('d/m/Y'),
            'data_vencimento' => Carbon::parse($ar_invoice['invoice_info']['date_invoice'])->addDays(7)->format('d/m/Y'),
            'proxima_data_vencimento' => $ar_invoice['invoice_info']['date_end'],
            'price' => $invoice_price,
            'payment_method' => $ar_invoice['customer_info']['customer_payment'],
            'description' => $array_descriptions,
            'invoice_id' => $newInvoice,
            'url_base' => url('/')
          ];
          // dispara um unico e-mail com todo o conteúdo
          \Mail::to($ar_invoice['customer_info']['customer_email'])->send(new \App\Mail\NewInvoice($details));
        }
      } // end foreach em cada customer
    } // end if getServiceDues
  }


  public function viewInvoice($id)
  {
    $result = Invoice::find($id);
    if ($result) {
      $getCustomer = DB::table('customers')->where('id', $result->customer_id)->first();

      return view('frontend.viewInvoice', compact('result', 'getCustomer'));
    } else {
      return view('frontend.home');
    }
  }
}

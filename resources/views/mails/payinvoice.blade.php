<!DOCTYPE html>
<html>

<head>
    <title>{{$details['title']}}</title>
    <style>
        table {
            border: 1px solid #DDD;
        }

        table th,
        table td {
            padding: 3px 5px;
        }

        table th {
            border-bottom: 1px solid #DDD;
            border-right: 1px solid #DDD;
        }

        table td {
            border-right: 1px solid #DDD;
        }

        table th:last-child,
        table td:last-child {
            border-right: 0;
        }
    </style>
</head>

<body>
    <h1>{{$details['title']}}</h1>
    <p>Olá {{$details['customer']}}, como vai?</p>
    <p>Este é a confirmação e recibo do pagamento da <b>Fatura #{{$details['invoice_id']}}</b>.</p>

    <p><b>Serviço(s) Contratado(s):</b></p>
    <ul>
        @foreach($details['description'] as $description)
        <li>{{$description}}</li>
        @endforeach
    </ul>

    <p>----------------------------------------- </p>
    <p><b>Data de Pagamento:</b> {{$details['data_pagamento']}} <br>
        <b>Valor Pago:</b> R$ {{$details['price']}} <br>
        <b>Situação:</b> {{$details['status_payment']}}</p>

    <p><b>Observação:</b> Este e-mail servirá como recibo para este pagamento.</p>
    <p>Qualquer dúvida estamos à disposição. <br>
        Desejamos um ótimo dia!</p>

    <p>WhatsApp: <a href="https://api.whatsapp.com/send?phone=5516992747526" target="_Blank">(16) 9.9274-7526</a> || E-mail: <a href="mailto:contato@innsystem.com.br">contato@innsystem.com.br</a></p>
</body>

</html>
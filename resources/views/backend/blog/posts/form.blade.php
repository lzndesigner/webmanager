<p><small><b>Campos obrigatórios (<span class="text-danger">*</span>)</b></small></p>
<input type="hidden" id="views" name="views" value="{{isset($result->views) ? $result->views : '0'}}">
<fieldset>
  <div class="form-row align-items-center">
    <div class="col-xs-12 col-md-7">
      <div class="form-group">
        <label for="title" class="col-form-label">Título (<span class="text-danger">*</span>):</label>
        <input type="text" id="title" name="title" class="form-control" placeholder="Título da postagem" value="{{isset($result->title) ? $result->title : ''}}">
      </div><!-- form-group -->
    </div><!-- col -->

    <div class="col-xs-12 col-md-3">
      <div class="form-group">
        <label for="category_id">Categoria:</label>
        <select name="category_id" id="category_id" class="form-control">
          <option disabled selected>Escolha uma</option>
          @foreach($getCategories as $getCategory)
          <option value="{{ $getCategory->id }}" @if(isset($result->category_id)) @if($getCategory->id == $result->category_id) selected @endif @endif >{{ $getCategory->name }}</option>
          @endforeach
        </select>
      </div>
    </div><!-- col -->

    <div class="col-xs-12 col-md-2">
      <div class="form-group">
        <label for="status">Status:</label>
        <select name="status" id="status" class="form-control">
          @if(isset($result->status))
          <option value="ativo" {{ $result->status == "ativo" ? 'selected' : ''}}>Ativo</option>
          <option value="desabilitado" {{ $result->status == "desabilitado" ? 'selected' : ''}}>Desabilitado</option>
          @else
          <option value="ativo" selected>Ativo</option>
          <option value="desabilitado">Desabilitado</option>
          @endif
        </select>
      </div>
    </div><!-- col -->
  </div><!-- form-row -->

  <div class="form-row">
    <div class="col-xs-12 col-md-12">
      <div class="form-group">
        <label for="slug" class="col-form-label">URL Amigável:</label>
        <input type="text" id="slug" name="slug" class="form-control" placeholder="URL Amigável" value="{{isset($result->slug) ? $result->slug : ''}}">
      </div><!-- form-group -->
    </div><!-- col -->
  </div><!-- form-row -->

  <div class="form-row">
    <div class="col-xs-12 col-md-12">
      <div class="form-group">
        <label for="description" class="col-form-label">Descrição:</label>
        <textarea id="description" name="description" class="form-control" placeholder="Faça uma descrição sobre o serviço. (opcional)" rows="3">{{isset($result->description) ? $result->description : ''}}</textarea>
      </div><!-- form-group -->
    </div><!-- col -->
  </div><!-- form-row -->
</fieldset>

<fieldset>

</fieldset>
@extends('backend.base')
@section('title', 'Cliente - '.$result->name)

@section('content')
<section>
  <div class="">
    <div class="row">
      <div class="col-xs-12 col-md-12">
        <div class="card">
          <div class="card-body">
            <h5 class="card-title"><a href="{{ url('admin/customers') }}" class="mr-2"><i class="fa fa-angle-left"></i></a> @yield('title')</h5>

            <hr>

            <ul class="nav nav-tabs" id="myTab" role="tablist">
              <li class="nav-item" role="presentation">
                <a class="nav-link active" href="{{ route('backend.customers.show', $result->id) }}" aria-controls="resume" aria-selected="true">Resumo</a>
              </li>
              <li class="nav-item" role="presentation">
                <a class="nav-link" href="{{ route('backend.customers.showServices', $result->id) }}" aria-controls="services" aria-selected="false">Serviços</a>
              </li>
              <li class="nav-item" role="presentation">
                <a class="nav-link" href="{{ route('backend.customers.showInvoices', $result->id) }}" aria-controls="invoices" aria-selected="false">Faturas</a>
              </li>
            </ul>
            <div class="tab-content pt-4" id="nav-tabContent">
              <div class="tab-pane active" id="resume" role="tabpanel" aria-labelledby="resume-tab">

              <!-- conteudo -->
                <div class="row">
                  <div class="col-xs-12 col-md-4">
                    <div class="card">
                      <div class="card-body">
                        <h5>Atividades Recentes</h5>
                        <div class="list-emails">
                          <ul class="list-unstyled list-details">
                            @if($myActivities)
                            @foreach($myActivities as $myActivitie)
                            <li><a href="javascript:;" class="btn-xs mx-0 px-0" style="cursor:default;"><i class="fa fa-plus"></i></a> {{$myActivitie->description}} <small class="mt-1 float-right">{{ Carbon\Carbon::parse($myActivitie->created_at)->format('d/m/Y H:i')}}</small></li>
                            @endforeach
                            @endif
                          </ul>
                        </div>
                      </div><!-- card-body -->
                    </div><!-- card -->
                  </div><!-- cols -->
                </div><!-- row -->
                <!-- conteudo -->
              </div><!-- tab-pane -->

            </div><!-- tab-content -->

          </div><!-- card-body -->
        </div><!-- card -->
      </div><!-- cols -->
    </div><!-- row -->
  </div><!--  -->
</section>
@endsection

@section('cssPage')
<link rel="stylesheet" href="{{ asset('/general/plugins/sweetalert/sweetalert2.min.css') }}">
@endsection

@section('jsPage')
<script>
  // Change Details User - Select Change
  $('#select_customer').on('change', function() {
    var data_id = $(this).val();
    location.href = `${data_id}`;
  });
</script>

<script src="{{ asset('/general/plugins/sweetalert/sweetalert2.min.js') }}"></script>
<script>
  // Open Modal - Create - Services
  $(document).on("click", "#button-create-service", function() {
    var customer_id = $(this).data('customer_id');
    $("#form-content-service").html('');
    $("#modalmyServices").modal('show');
    var url = `{{url('admin/customerservices-create/${customer_id}')}}`;
    console.log(url);
    $.get(url,
      $(this)
      .addClass('modal-scrollfix')
      .find('#form-content-service')
      .html('Carregando...'),
      function(data) {
        // console.log(data);
        $("#form-content-service").html(data);
        $("#btn-salvar-service").attr('data-type', 'create');
        formatedDate();
        formatedPrice();
        formatedPhone();
        // aqui quando selecionar um serviço, buscar qual o valor dele e atualizar o campo de preço.
        $('#service_id').on('change', function() {
          var service_id = $(this).val();
          var service_price = $(this).find(':selected').data('price');
          $('#price').val(service_price);
        });
      });
  });

  // Open Modal - Edit - Services
  $(document).on("click", "#button-edit-service", function() {
    let id = $(this).data('id');
    var customer_id = $(this).data('customer_id');
    $("#form-content-service").html('');
    $("#modalmyServices").modal('show');
    var url = `{{url('admin/customerservices-edit/${customer_id}/${id}/')}}`;
    $.get(url,
      $(this)
      .addClass('modal-scrollfix')
      .find('#form-content-service')
      .html('Carregando...'),
      function(data) {
        // console.log(data);
        $("#form-content-service").html(data);
        $("#btn-salvar-service").attr('data-type', 'edit').attr('data-id', id);
        formatedDate();
        // formatedPrice();
        formatedPhone();
        $('#group-generate-invoice').addClass('d-none');
        $('#service_id').attr("style", "pointer-events: none;");
      });
  });


  // Button Save Forms - Create and Edit - Services
  $(document).on('click', '#btn-salvar-service', function(e) {
    e.preventDefault();
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': "{{csrf_token()}}"
      }
    });

    let id = $(this).data('id');
    var type = $(this).data('type');

    if (type == 'edit') {
      if (id) {
        var url = `{{ url('admin/customerservices-update/${id}') }}`;
        var method = 'PUT';
      }
    } else {
      var url = "{{ url('admin/customerservices-store') }}";
      var method = 'POST';
    }
    var data = $('#form-request-service').serialize();
    $.ajax({
      url: url,
      data: data,
      method: method,
      success: function(data) {
        Swal.fire({
          text: data,
          icon: 'success',
          showClass: {
            popup: 'animate_animated animate_backInUp'
          },
          onClose: () => {
            // Loading page listagem
            // location.href = "{{url($url_action)}}";
            location.reload();
          }
        });
      },
      error: function(xhr) {
        if (xhr.status === 422) {
          Swal.fire({
            text: 'Validação: ' + xhr.responseJSON,
            icon: 'warning',
            showClass: {
              popup: 'animate_animated animate_wobble'
            }
          });
        } else {
          Swal.fire({
            text: 'Erro interno, informe ao suporte: ' + xhr.responseJSON,
            icon: 'error',
            showClass: {
              popup: 'animate_animated animate_wobble'
            }
          });
        }
      }
    });
  });


  // Button - Delete - Services
  $('#btn-delete-service').click(function(e) {
    Swal.fire({
      title: 'Deseja remover este registro?',
      text: "Você não poderá reverter isso!",
      icon: 'question',
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sim, deletar!'
    }).then((result) => {
      if (result.value) {
        $.ajax({
          url: "{{url('admin/customerservices-delete')}}",
          method: 'DELETE',
          data: $('#form-table-service').serialize(),
          success: function(data) {
            // Loading page listagem
            // location.href = "{{url($url_action)}}";
            location.reload();
          },
          error: function(xhr) {
            if (xhr.status === 422) {
              Swal.fire({
                text: xhr.responseJSON,
                icon: 'warning',
                showClass: {
                  popup: 'animate_animated animate_wobble'
                }
              });
            } else {
              Swal.fire({
                text: xhr.responseJSON,
                icon: 'error',
                showClass: {
                  popup: 'animate_animated animate_wobble'
                }
              });
            }
          }
        });
      }
    });

  });
</script>


<script>
  // Open Modal - Create - Invoices
  $(document).on("click", "#button-create-invoice", function() {
    var customer_id = $(this).data('customer_id');
    $("#form-content-invoice").html('');
    $("#modalmyInvoices").modal('show');
    var url = `{{url('admin/invoices-create/${customer_id}')}}`;
    console.log(url);
    $.get(url,
      $(this)
      .addClass('modal-scrollfix')
      .find('#form-content-invoice')
      .html('Carregando...'),
      function(data) {
        // console.log(data);
        $("#form-content-invoice").html(data);
        $("#btn-salvar-invoice").attr('data-type', 'create');
        formatedDate();
        formatedPrice();
        formatedPhone();
      });
  });

  // Open Modal - Edit - Invoices
  $(document).on("click", "#button-edit-invoice", function() {
    let id = $(this).data('id');
    var customer_id = $(this).data('customer_id');
    $("#form-content-invoice").html('');
    $("#modalmyInvoices").modal('show');
    var url = `{{url('admin/invoices-edit/${customer_id}/${id}/')}}`;
    $.get(url,
      $(this)
      .addClass('modal-scrollfix')
      .find('#form-content-invoice')
      .html('Carregando...'),
      function(data) {
        // console.log(data);
        $("#form-content-invoice").html(data);
        $("#btn-salvar-invoice").attr('data-type', 'edit').attr('data-id', id);
        formatedDate();
        // formatedPrice();
        formatedPhone();

        // Button Confirm Payment Invoice
        $(document).on('click', '#button-confirm-payment', function(e) {
          e.preventDefault();
          $.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': "{{csrf_token()}}"
            }
          });

          let id_invoice = $(this).data('id-invoice');


          if (id_invoice) {
            var url = `{{ url('admin/invoices-confirm/${id_invoice}') }}`;
            var method = 'PUT';
          }

          var data = $('#form-request-invoice-confirm').find('input:checked');
          console.log(data);

          $.ajax({
            url: url,
            data: data,
            method: method,
            success: function(data) {
              Swal.fire({
                text: data,
                icon: 'success',
                showClass: {
                  popup: 'animate_animated animate_backInUp'
                },
                onClose: () => {
                  // Loading page listagem
                  // location.href = "{{url($url_action)}}";
                  location.reload();
                }
              });
            },
            error: function(xhr) {
              if (xhr.status === 422) {
                Swal.fire({
                  text: 'Validação: ' + xhr.responseJSON,
                  icon: 'warning',
                  showClass: {
                    popup: 'animate_animated animate_wobble'
                  }
                });
              } else {
                Swal.fire({
                  text: 'Erro interno, informe ao suporte: ' + xhr.responseJSON,
                  icon: 'error',
                  showClass: {
                    popup: 'animate_animated animate_wobble'
                  }
                });
              }
            }
          });
        });
        // end button Confirm Payment Service

      });
  });


  // Button Save Forms - Create and Edit - Invoices
  $(document).on('click', '#btn-salvar-invoice', function(e) {
    e.preventDefault();
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': "{{csrf_token()}}"
      }
    });

    let id = $(this).data('id');
    var type = $(this).data('type');

    if (type == 'edit') {
      if (id) {
        var url = `{{ url('admin/invoices-update/${id}') }}`;
        var method = 'PUT';
      }
    } else {
      var url = "{{ url('admin/invoices-store') }}";
      var method = 'POST';
    }
    var data = $('#form-request-invoice').serialize();
    $.ajax({
      url: url,
      data: data,
      method: method,
      success: function(data) {
        Swal.fire({
          text: data,
          icon: 'success',
          showClass: {
            popup: 'animate_animated animate_backInUp'
          },
          onClose: () => {
            // Loading page listagem
            // location.href = "{{url($url_action)}}";
            location.reload();
          }
        });
      },
      error: function(xhr) {
        if (xhr.status === 422) {
          Swal.fire({
            text: 'Validação: ' + xhr.responseJSON,
            icon: 'warning',
            showClass: {
              popup: 'animate_animated animate_wobble'
            }
          });
        } else {
          Swal.fire({
            text: 'Erro interno, informe ao suporte: ' + xhr.responseJSON,
            icon: 'error',
            showClass: {
              popup: 'animate_animated animate_wobble'
            }
          });
        }
      }
    });
  });


  // Button - Delete - Invoices
  $('#btn-delete-invoice').click(function(e) {
    Swal.fire({
      title: 'Deseja remover este registro?',
      text: "Você não poderá reverter isso!",
      icon: 'question',
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sim, deletar!'
    }).then((result) => {
      if (result.value) {
        $.ajax({
          url: "{{url('admin/invoices-delete')}}",
          method: 'DELETE',
          data: $('#form-table-invoice').serialize(),
          success: function(data) {
            // Loading page listagem
            // location.href = "{{url($url_action)}}";
            location.reload();
          },
          error: function(xhr) {
            if (xhr.status === 422) {
              Swal.fire({
                text: xhr.responseJSON,
                icon: 'warning',
                showClass: {
                  popup: 'animate_animated animate_wobble'
                }
              });
            } else {
              Swal.fire({
                text: xhr.responseJSON,
                icon: 'error',
                showClass: {
                  popup: 'animate_animated animate_wobble'
                }
              });
            }
          }
        });
      }
    });

  });
</script>
@endsection
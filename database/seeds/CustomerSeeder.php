<?php

use Illuminate\Database\Seeder;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customers')->delete();

        DB::table('customers')->insert([
            'name' => 'Leonardo Augusto',
            'company' => 'InnSystem',
            'email' => 'donoleozinnn@live.com',
            // 'password' => Hash::make('password'),
            'password' => bcrypt('123456'),
            'remember_token' => Str::random(10),
            'status' => 'Ativo',
            'cep' => '14077-220',
            'address' => 'Avenida Antonio da Costa Lima',
            'number' => '526',
            'complement' => 'Casa 1',
            'city' => 'Ribeirão Preto',
            'state' => 'São Paulo',
            'phone' => '(16) 9.9274-7526',
            'payment_method' => 'mercadopago',
            'balance_account' => '0.05',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);

    }
}

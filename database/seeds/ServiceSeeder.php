<?php

use Illuminate\Database\Seeder;

class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('services')->insert([
            'name' => 'Loja Virtual',
            'price' => '100.00',
            'price_trimestral' => '300.00',
            'price_anual' => '1200.00',
            'period' => 'recorrente',
            'status' => 'Ativo',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);

    }
}

<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    DB::table('users')->delete();

    DB::table('users')->insert([
      'name' => 'Leonardo Augusto',
      'email' => 'contato@innsystem.com.br',
      'password' => bcrypt('@lzn94designer@'),
      'remember_token' => Str::random(10),
    ]);
  }
}

<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/************************/
// Routes Frontend
Route::get('/', 'Frontend\HomeController@index')->name('frontend.home');
Route::get('/fatura/{id}', 'Frontend\HomeController@viewInvoice')->name('frontend.invoice.show');

Auth::routes(['register' => false]);
/************************/
// Routes Backend
Route::prefix('admin')->middleware('auth')->group(function () {
    Route::get('/', 'Backend\HomeController@index')->name('backend.home');

    //Customers
    Route::get('/customers', 'Backend\CustomerController@index')->name('backend.customers');
    Route::get('/customers-create', 'Backend\CustomerController@create')->name('backend.customers.create');
    Route::post('/customers-store', 'Backend\CustomerController@store')->name('backend.customers.store');
    Route::get('/customers-edit/{id}', 'Backend\CustomerController@edit')->name('backend.customers.edit');
    Route::put('/customers-update/{id}', 'Backend\CustomerController@update')->name('backend.customers.update');
    Route::get('/customers-details/{id}', 'Backend\CustomerController@show')->name('backend.customers.show');
    Route::get('/customers-services/{id}', 'Backend\CustomerController@showServices')->name('backend.customers.showServices');
    Route::get('/customers-invoices/{id}', 'Backend\CustomerController@showInvoices')->name('backend.customers.showInvoices');
    Route::get('/customers-activities/{id}', 'Backend\CustomerController@showActivities')->name('backend.customers.showActivities');
    Route::delete('/customers-delete', 'Backend\CustomerController@destroy')->name('backend.customers.delete');

    //Customer Services
    Route::get('/customerservices-create/{customer_id}', 'Backend\CustomerServicesController@create')->name('backend.customerservices.create');
    Route::post('/customerservices-store', 'Backend\CustomerServicesController@store')->name('backend.customerservices.store');
    Route::get('/customerservices-edit/{customer_id}/{id}', 'Backend\CustomerServicesController@edit')->name('backend.customerservices.edit');
    Route::put('/customerservices-update/{id}', 'Backend\CustomerServicesController@update')->name('backend.customerservices.update');
    Route::get('/customerservices-details/{id}', 'Backend\CustomerServicesController@show')->name('backend.customerservices.show');
    Route::delete('/customerservices-delete', 'Backend\CustomerServicesController@destroy')->name('backend.customerservices.delete');

    //Customer Invoices
    Route::get('/invoices', 'Backend\InvoicesController@index')->name('backend.invoices');    
    Route::get('/invoices-create/{customer_id}', 'Backend\InvoicesController@create')->name('backend.invoices.create');
    Route::post('/invoices-store', 'Backend\InvoicesController@store')->name('backend.invoices.store');
    Route::get('/invoices-edit/{customer_id}/{id}', 'Backend\InvoicesController@edit')->name('backend.invoices.edit');
    Route::put('/invoices-update/{id}', 'Backend\InvoicesController@update')->name('backend.invoices.update');
    Route::put('/invoices-confirm/{id}', 'Backend\InvoicesController@confirm')->name('backend.invoices.confirm');
    Route::get('/invoices-details/{id}', 'Backend\InvoicesController@show')->name('backend.invoices.show');
    Route::delete('/invoices-delete', 'Backend\InvoicesController@destroy')->name('backend.invoices.delete');
    
    //Services
    Route::get('/services', 'Backend\ServiceController@index')->name('backend.services');
    Route::get('/services-create', 'Backend\ServiceController@create')->name('backend.services.create');
    Route::post('/services-store', 'Backend\ServiceController@store')->name('backend.services.store');
    Route::get('/services-edit/{id}', 'Backend\ServiceController@edit')->name('backend.services.edit');
    Route::put('/services-update/{id}', 'Backend\ServiceController@update')->name('backend.services.update');
    Route::get('/services-details/{id}', 'Backend\ServiceController@show')->name('backend.services.show');
    Route::delete('/services-delete', 'Backend\ServiceController@destroy')->name('backend.services.delete');
    
    Route::get('/reports', 'Backend\HomeController@reports')->name('backend.reports');
    
    
    //Blog - Categories
    Route::get('/blog/categories', 'Backend\BlogCategoriesController@index')->name('backend.blog.categories');    
    Route::get('/blog/categories-create', 'Backend\BlogCategoriesController@create')->name('backend.blog.categories.create');
    Route::post('/blog/categories-store', 'Backend\BlogCategoriesController@store')->name('backend.blog.categories.store');
    Route::get('/blog/categories-edit/{id}', 'Backend\BlogCategoriesController@edit')->name('backend.blog.categories.edit');
    Route::put('/blog/categories-update/{id}', 'Backend\BlogCategoriesController@update')->name('backend.blog.categories.update');
    Route::get('/blog/categories-details/{id}', 'Backend\BlogCategoriesController@show')->name('backend.blog.categories.show');
    Route::delete('/blog/categories-delete', 'Backend\BlogCategoriesController@destroy')->name('backend.blog.categories.delete');
    //Blog - Posts
    Route::get('/blog/posts', 'Backend\BlogPostsController@index')->name('backend.blog.posts');    
    Route::get('/blog/posts-create', 'Backend\BlogPostsController@create')->name('backend.blog.posts.create');
    Route::post('/blog/posts-store', 'Backend\BlogPostsController@store')->name('backend.blog.posts.store');
    Route::get('/blog/posts-edit/{id}', 'Backend\BlogPostsController@edit')->name('backend.blog.posts.edit');
    Route::put('/blog/posts-update/{id}', 'Backend\BlogPostsController@update')->name('backend.blog.posts.update');
    Route::get('/blog/posts/{slugCategory}/{slugPost}', 'Backend\BlogPostsController@show')->name('backend.blog.posts.show');
    Route::delete('/blog/posts-delete', 'Backend\BlogPostsController@destroy')->name('backend.blog.posts.delete');


    
    Route::get('/settings', 'Backend\HomeController@settings')->name('backend.settings');
    
    // Desativado function copy/duplique
    // Route::post('/portfolios/copy', 'PortfolioController@copy');
});
